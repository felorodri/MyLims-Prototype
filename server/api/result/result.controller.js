/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/results              ->  index
 * GET     /api/results/:id          ->  show
 * POST    /api/results              ->  create
 * POST    /api/results/search       ->  searchResults
 * PUT     /api/results/:id          ->  update
 * DELETE  /api/results/:id          ->  destroy
 **/

'use strict';

import _ from 'lodash';
var Result = require('./result.model');
var Spectra = require('../spectra/spectra.model');
var Data = require('../data/data.model');
var mongoose = require('mongoose');

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

// Gets a list of Results
export function index(req, res) {
  Result.findAsync()
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets a single Result from the DB
export function show(req, res) {
  Result.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Creates a new Result in the DB
export function create(req, res) {
  Result.createAsync(req.body)
    .then(responseWithResult(res, 201))
    .catch(handleError(res));
}

// Search into results by a given parameters
export function searchResults(req,res){

  try{

    var user_id = req.body.user_id;
    var type   = req.body.type;
    var request_code = req.body.request_code;
    var in_result_data = req.body.in_result_data;
    var query = req.body.query;
    var spectraQuery  = {};
    
    if (!(user_id === undefined)){

      if (request_code !== undefined) spectraQuery.request_code = new RegExp('^' + request_code + '$', "i");

      if (type !== undefined){

        if(type.mine === true){

          if (!(type.shared === true)) {

            spectraQuery.owner_id = user_id;
            spectraQuery.shared_to = {$ne: user_id};

            if (type.public) {
              spectraQuery.public = type.public;
            }
            
          }else{

            spectraQuery['$or'] = [ { owner_id: user_id }, { shared_to: user_id } ];
            
            if (type.public) {
              spectraQuery.public = type.public;
            }

          }

        }else{

          if (type.shared === true) {

            spectraQuery.owner_id = {$ne: user_id};
            spectraQuery.shared_to = user_id;
            spectraQuery.public = type.public;            

          }

        }

      }
      
      Spectra.find(spectraQuery, {_id:0, experiments:1})//.where(spectraQuery)
      .execAsync()
      .then(handleEntityNotFound(res))
      .then( function (spectra_data){

        var results_ids = [];
        
        for (var i = 0; i < spectra_data.length; i++) {
          
          var experiments = spectra_data[i].toJSON().experiments; 
          
          for (var k = 0; k < experiments.length; k++) {
            
            results_ids = results_ids.concat(experiments[k].results);
            
          }
          
        }


        if (!(query === undefined)){


          if (in_result_data){

            /*************************** SEARCH IN RESUTLS DATA FILES **************************/

            if ( !(query.on_data === undefined) && !(query.on_result === undefined)) {

              var query_array = query.on_result;
              var query_on_data = query.on_data;
              var mongoDB_query = (parser(query_array));
              var mongo_results_ids = [];
              
              for (var z = 0; z < results_ids.length; z++) {
                
                mongo_results_ids.push(mongoose.Types.ObjectId(results_ids[z]));  

              }

              mongoDB_query._id = {'$in': mongo_results_ids};
              
              Result.find(mongoDB_query,{_id:1, data:1})//.where(spectraQuery)
              .execAsync()
              .then(handleEntityNotFound(res))
              .then(function (result_data){

                var datas_id = [];
                
                for (var c = 0; c < result_data.length; c++) {
                  datas_id.push(result_data[c].toJSON().data);
                }

                Data.find({_id: {'$in': datas_id}})//.where(spectraQuery)
                .execAsync()
                .then(handleEntityNotFound(res))
                .then(function (data){
                  
                  var matched_data = [];
                                    
                  for (var i = 0; i < data.length; i++) {
                                      
                    if ( queryEvalution(query_on_data, data[i].toJSON().text, data[i].toJSON()._id ) ) {
                      
                      matched_data.push(data[i].toJSON()._id );

                    }

                  }

                  if ( matched_data.length > 0 ) {

                    Result.find({data: {'$in': matched_data}},{_id:1})//.where(spectraQuery)
                    .execAsync()
                    .then(handleEntityNotFound(res))
                    .then(function (matched_result_data){

                      var matched_ids = [];

                      for (var x = 0; x < matched_result_data.length; x++) {
                        
                        matched_ids.push(matched_result_data[x]._id);

                      }

                      Spectra.find({"experiments.results": {'$in': matched_ids}})//.where(spectraQuery)
                      .execAsync()
                      .then(handleEntityNotFound(res))
                      .then( function (matched_spectras){
                        
                        res.status(200).json(matched_spectras);  

                      });
            

                    });

                  }else{
                      
                      res.status(200).json(matched_data);

                  }

                })
                .catch(handleError(res, 400)); 

              })
              .catch(handleError(res, 500)); 


            }else{

              throw 'No query defined';

            }
          
          }else{

            /***************************** SEARCH IN RESUTLS ****************************/
            
            var query_array = query.on_result;
            var mongoDB_query = (parser(query_array));
            var mongo_results_ids = [];
            
            for (var z = 0; z < results_ids.length; z++) {
              
              mongo_results_ids.push(mongoose.Types.ObjectId(results_ids[z]));  

            }

            mongoDB_query._id = {'$in': mongo_results_ids};

            Result.find(mongoDB_query)//.where(spectraQuery)
            .execAsync()
            .then(handleEntityNotFound(res))
            .then(function (result_data){

              var matched_ids = [];

              for (var x = 0; x < result_data.length; x++) {
                        
                matched_ids.push(result_data[x]._id);

              }

              Spectra.find({"experiments.results": {'$in': matched_ids}})//.where(spectraQuery)
              .execAsync()
              .then(handleEntityNotFound(res))
              .then( function (matched_spectras){
                       
                res.status(200).json(matched_spectras);  

              });

            })
            .catch(handleError(res, 500));          
            
          }

        }else{
          
          Result.find({_id: {'$in' : results_ids}})//.where(spectraQuery)
          .execAsync()
          .then(handleEntityNotFound(res))
          .then(function (result_data){
            // console.log(result_data);
            res.status(200).json(result_data);  
          })
          .catch(handleError(res, 500));
          
        }

      })
      .catch(handleError(res, 400));

    }else{

      throw 'No user id provided';

    }

  }catch(err){

    handleError(res, 400)(err);
    
  }

}

// Updates an existing Result in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Result.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Deletes a Result from the DB
export function destroy(req, res) {
  Result.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}


// Parses the request query data and return the MongoDB query object
function parser (query_array){
  
  if (query_array.length === 0) {

    return {};

  }else if (query_array.length === 1) {
              
    var string = query_array[0];

    if ( string.indexOf("=") > -1 ){
                
      var data = string.split('=');

      if(isNaN(data[1])){
        var response = {};
        response[data[0]] = data[1];
        return response;

      }else{

        var response = {};
        response[data[0]] =  Number(data[1]);
        return response;

      }
                
    }else if ( string.indexOf("<") > -1 ){

      var data = string.split('<');

      if(isNaN(data[1])){
                  
        throw 'No number provided';

      }else{

        var response = {};
        response[data[0]] =  {'$lt': Number(data[1])};
        return response;

      }

    }else if (string.indexOf(">") > -1) {

      var data = string.split('>');

      if(isNaN(data[1])){
                  
        throw 'No number provided';

      }else{

        var response = {};
        response[data[0]] =  {'$gt': Number(data[1])};
        return response;

      }                

    }else{

      throw 'Unexpected error';

    }
            
  }else if (query_array.length === 2){

    var string = query_array[1][0];

    if ( string.indexOf("=") > -1 ){
                
      var data = string.split('=');

      if(isNaN(data[1])){
                  
        var response = {};
        response[data[0]] = {'$ne': data[1]};
        return response;

      }else{

        var response = {};
        response[data[0]] =  {'$ne': Number(data[1])};
        return response;

      }
                
    }else{

      throw '';

    }

  }else{
              
    var response = {};

    var op = query_array[0];

    if (op === "OR" || op === "oR" || op === "Or" || op === "or"){
                
      response['$or'] = [];
                
      for (var n = 1; n < query_array.length; n++) {
                  
        response['$or'].push(parser(query_array[n]));

      }


    }else if (op === "AND" || op === "aND" || op === "anD" || op === "and" || op === "And" || op === "ANd" || op === "AnD") { 

      var vars = [];
      var and_response = {};

      for (var d = 1; d < query_array.length; d++) {
                  
        vars.push(parser(query_array[d]));

      }

      for (var l = 0; l < vars.length; l++) {
                  
        var keys = Object.keys(vars[l]);
                  
        for (var m = 0; m < keys.length; m++){

          and_response[keys[m]] = vars[l][keys[m]];

        }

      }

      response = and_response;

    }else{

      throw 'No operator given';

    }

    return response;

  }

}

// Evaluate the on data query expressions
function queryEvalution (query_array, text, data_id){

  if (query_array.length < 1) {

    throw 'Wrong array length';

  }
  
  var string = query_array[0];
  var number = false;
  var op = '';
  var data = [];

  if (query_array.length === 1) {
      
    if ( string.indexOf("=") > -1 ){
                      
      data = string.split('=');
                                            
      if(!isNaN(data[1])){
                        
        data[1] = Number(data[1]);
        number = true;
                                               
      }

      op = '=';

    }else if ( string.indexOf("<") > -1 ){

      data = string.split('<');

      if(isNaN(data[1])){
                                      
        throw 'Expecting a number';

      }else{

        data[1] = Number(data[1]);
        number = true;
        op = '<'

      }

    }else if (string.indexOf(">") > -1) {

      data = string.split('>');

      if(isNaN(data[1])){
                                      
        throw 'Expecting a number';

      }else{

        data[1] = Number(data[1]);
        number = true;
        op = '>'
                        
      }                

    }else{

      throw 'No numeric operator given';

    }

    
    var word_ocurrences = new RegExp(data[0]+'=', 'gi');
    
    if (text.match(word_ocurrences) === null) {

      return false;

    }else if ((text.match(word_ocurrences).length) > 1){
                      
      // throw 'Too many ocurrences of '+ data[0] + ' in the result data file' ;

      var content_array = text.match(/[^\r\n]+/g);
      var matched_string = [];
      var matches_found = 0;
      var key = "";
      var vals = [];

      for (var k = 0; k < content_array.length; k++) {
                         
        if ( (content_array[k].indexOf(data[0]+'=')) > -1 ) {
      
          matched_string.push(content_array[k]);
          matches_found ++;
          
          if(matches_found == text.match(word_ocurrences).length){
            break;  
          }
      
        }

      }


      for (var h = 0; h < matched_string.length; h++){

        var text_line = matched_string[h].replace(/#/g, '');  
        text_line = text_line.replace(/\./g,'');
        text_line = text_line.replace(/\$/g,'');
        text_line = text_line.replace(/\ /g,'');
        var values = text_line.split("=");

        if(h === 0) key = values[0];

        if (number) {
         
          vals.push(Number(values[1]));

        }else{

          vals.push(values[1]);

        }

      }

      Result.findOne({ data: mongoose.Types.ObjectId(data_id) }, function (err, doc){
        
        if (!err){

          doc.set('other_params.'+key, vals);
          doc.save();

        }else{

          console.log(err);

        }
               

      });


      var response = false;


      for (var j = 0; j < vals.length; j++) {
        
        if (op === '='){

          if (number){

            try{

              if (Number(vals[j]) === Number(data[1])){
                response = true;
                break;
              }

            }catch(err){
              throw err;
            }

          }else{

            if (vals[j] === data[1]){
              response = true;
              break;
            }
            
          }

        }else if (op === '<'){

          try{

            if (Number(vals[j]) < Number(data[1])){
              response = true;
              break;
            }

          }catch(err){
            throw err;
          }

        }else if (op === '>'){

          try{
              
            if (Number(vals[j]) > Number(data[1])){
              response = true;
              break;
            }

          }catch(err){
            throw err;
          }

        }else {

          throw 'No numeric operator given';

        }

      };

      return response;
      
    }else{

      var content_array = text.match(/[^\r\n]+/g);
      var matched_string = "";

      for (var k = 0; k < content_array.length; k++) {
                         
        if ( (content_array[k].indexOf(data[0])) > -1 ) {
      
          matched_string = content_array[k];
          break;
      
        }

      }

      var text_line = matched_string.replace(/#/g, '');
      text_line = text_line.replace(/\./g,'');
      text_line = text_line.replace(/\$/g,'');
      text_line = text_line.replace(/\ /g,'');

      var values = text_line.split("=");
      
      Result.findOne({ data: mongoose.Types.ObjectId(data_id) }, function (err, doc){
        
        if (!err){

          if (number) {
         
            doc.set('other_params.'+values[0], Number(values[1]));

          }else{

            doc.set('other_params.'+values[0], values[1]);

          }
            
          doc.save();

        }else{

          console.log(err);

        }

      });

      if (op === '='){

        if (number){

          try{

            if (Number(data[1]) === Number(values[1])){
              return true;
            }else{
              return false;
            }

          }catch(err){
            throw err;
          }

        }else{

          if (data[1] === values [1]){
            return true;
          }else{
            return false;
          }

        }

      }else if (op === '<'){

        try{

          if (Number(data[1]) > Number(values[1])){
            return true;
          }else{
            return false;
          }

        }catch(err){
          throw err;
        }

      }else if (op === '>'){

        try{

          if (Number(data[1]) < Number(values[1])){
            return true;
          }else{
            return false;
          }

        }catch(err){
          throw err;
        }

      }else {

        throw 'No numeric operator given';

      }
              
    }   

  }else if (query_array.length === 2){

    var operator = query_array[0];

    if (operator === "OR" || operator === "oR" || operator === "Or" || operator === "or"){

      return ( queryEvalution(query_array[1],text, data_id) || false );
                                           
    }else if (operator === "AND" || operator === "aND" || operator === "anD" || operator === "and" || operator === "And" || operator === "ANd" || operator === "AnD") { 

      return ( queryEvalution(query_array[1], text, data_id) && true );

    }else if (operator === "NOT" || operator === "nOT" || operator === "noT" || operator === "not" || operator === "Not" || operator === "NOt" || operator === "NoT") {

      return !queryEvalution(query_array[1], text, data_id);

    }else{

      throw 'No logic operator given';

    }
                   
  }else{
                              
    var operator = query_array[0];

    if (operator === "OR" || operator === "oR" || operator === "Or" || operator === "or"){
                      
      // query_array.shift();
      var query_array_buffer = query_array.slice(1);
      // var first_object = query_array.shift();
      return ( queryEvalution(query_array_buffer[0], text, data_id) || queryEvalution(["OR"].concat(query_array_buffer.slice(1)), text, data_id) );  
                      
    }else if (operator === "AND" || operator === "aND" || operator === "anD" || operator === "and" || operator === "And" || operator === "ANd" || operator === "AnD") { 

      // query_array.shift();
      var query_array_buffer = query_array.slice(1);
      return ( queryEvalution(query_array_buffer[0], text, data_id) && queryEvalution((["AND"]).concat(query_array_buffer.slice(1)), text, data_id) );
                                         
    }else{

      throw 'No logic operator given';

    }

  }

}