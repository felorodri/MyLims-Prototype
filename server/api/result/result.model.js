'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var ResultSchema = new mongoose.Schema({
  // name: String,
  // info: String,
  // active: Boolean
}, { strict: false });

export default mongoose.model('Result', ResultSchema);
