/**
 * Spectra model events
 */

'use strict';

import {EventEmitter} from 'events';
var Spectra = require('./spectra.model');
var SpectraEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
SpectraEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Spectra.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    SpectraEvents.emit(event + ':' + doc._id, doc);
    SpectraEvents.emit(event, doc);
  }
}

export default SpectraEvents;
