// 'use strict';

var express = require('express');
var controller = require('./spectra.controller');

var router = express.Router();

router.get('/', controller.all);

router.get('/public', controller.publicSpectras);
router.get('/shared/:id', controller.sharedSpectras);
router.get('/count', controller.count);
router.get('/public/count', controller.publicCount);
router.get('/:id', controller.owned);
router.get('/findone/:id', controller.findOne);

router.post('/', controller.create);
router.post('/search', controller.searchSpectras);

router.put('/share/:id', controller.shareSpectra);
router.put('/:id', controller.update);

router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;
