/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/spectras              ->  all
 * GET     /api/spectras/:id          ->  owned
 * GET     /api/spectras/findone/:id  ->  findOne
 * GET     /api/spectras/shared/:id   ->  sharedSpectras
 * POST    /api/spectras              ->  create
 * POST    /api/spectras/search       ->  searchSpectras
 * PUT     /api/spectras/:id          ->  update
 * PUT     /api/spectras/share/:id    ->  shareSpectra
 * DELETE  /api/spectras/:id          ->  destroy
 **/

'use strict';

import _ from 'lodash';
var Spectra = require('./spectra.model');

function handleError(res, statusCode) {
  
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      res.status(statusCode).json(entity);
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.saveAsync()
      .spread(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

// Gets a list of Spectras
export function all(req, res) {
  Spectra.findAsync()
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets a single Spectra from the DB
export function owned(req, res) {
  Spectra.find().where({owner_id: req.params.id}).sort({created_at: -1})
    .execAsync()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets a single Spectra from the DB
export function findOne(req, res) {
  Spectra.find({_id: req.params.id})//.where({owner_id: req.params.id})
    .execAsync()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets all spectras shared spectras for a given email id
export function sharedSpectras(req, res) {
  Spectra.find().where({shared_to: req.params.id})
    .execAsync()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets all public spectras 
export function publicSpectras(req, res) {
  Spectra.find({public: true})/*.where({public: true})*/
    .execAsync()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Creates a new Spectra in the DB
export function create(req, res) {
  Spectra.createAsync(req.body)
    .then(responseWithResult(res, 201))
    .catch(handleError(res));
}

// Search spetra by custom params
export function searchSpectras(req,res){


  try {
      
    var user_id = req.body.user_id;
    var type   = req.body.type;
    var request_code = req.body.request_code;
    var experiments  = req.body.experiments;
    var strict = req.body.strict;
    
    var query   = {};

    if (!(user_id === undefined)){

      if (request_code !== undefined) query.request_code = new RegExp('^' + request_code + '$', "i");

      if (type !== undefined){

        if(type.mine === true){

          if (!(type.shared === true)) {

            query.owner_id = user_id;
            query.shared_to = {$ne: user_id};

            if (type.public) {
              query.public = type.public;
            }
            
          }else{

            query['$or'] = [ { owner_id: user_id }, { shared_to: user_id } ];
            
            if (type.public) {
              query.public = type.public;
            }

          }

        }else{

          if (type.shared === true) {

            query.owner_id = {$ne: user_id};
            query.shared_to = user_id;
            query.public = type.public;            

          }

        }

      }

      if (experiments !== undefined){

        var tmp_experiments = [];
        
        for (var i = 0; i < experiments.length; i++) {
          
          var exp_json = experiments[i];

          query['experiments.technique'] = exp_json.technique;

          if (exp_json.exp !== undefined){

            if(strict){

              if (exp_json.exp.length > 0){
                query['experiments.experiments'] = experiments[i].exp;
              }
              
            }else{

              tmp_experiments.concat(experiments[i].exp);
              
              if (i === (experiments.length-1)){

                if (tmp_experiments.length > 0){

                  query['experiments.experiments'] = {'$in': tmp_experiments };

                }

              }
              
            }

          }
          
        }

      }
      
      Spectra.find(query)//.where(query)
      .execAsync()
      .then(handleEntityNotFound(res))
      .then(responseWithResult(res))
      .catch(handleError(res));

    }else{

      throw 'No user id provided';

    }

  }catch(err){

    handleError(res, 400)(err);
    
  }
}

// Updates an existing Spectra in the DB
export function update(req, res) {

  if (req.body._id) {
    delete req.body._id;
  }

  Spectra.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(responseWithResult(res))
    .catch(handleError(res));
}


// Contact an email list whith the existing email list of a document found by id
export function shareSpectra(req, res) {

  Spectra.findByIdAsync(req.params.id)//.where(query)
      .then(handleEntityNotFound(res))
      .then( function (doc){
        
        doc.shared_to = (doc.shared_to).concat(req.body.emails);
        doc.save(function (err){
          
          if (!err){
             res.status(200).send();
          }else{
            throw '';
          }

        });

      })
      .catch(handleError(res));
 
}

// Deletes a Spectra from the DB
export function destroy(req, res) {
  Spectra.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

// Returns the number of documents in spectra collection 
export function count(req, res){
  
  Spectra.count()/*.where({public: true})*/
    .execAsync()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));

}

// Returns the number of documents in spectra collection where the flag public is true
export function publicCount(req, res){
  
  Spectra.count({public:true})/*.where({public: true})*/
    .execAsync()
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));

}
