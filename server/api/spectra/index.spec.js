'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var spectraCtrlStub = {
  index: 'spectraCtrl.index',
  show: 'spectraCtrl.show',
  create: 'spectraCtrl.create',
  update: 'spectraCtrl.update',
  destroy: 'spectraCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var spectraIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './spectra.controller': spectraCtrlStub
});

describe('Spectra API Router:', function() {

  it('should return an express router instance', function() {
    expect(spectraIndex).to.equal(routerStub);
  });

  describe('GET /api/spectras', function() {

    it('should route to spectra.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'spectraCtrl.index')
        ).to.have.been.calledOnce;
    });

  });

  describe('GET /api/spectras/:id', function() {

    it('should route to spectra.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'spectraCtrl.show')
        ).to.have.been.calledOnce;
    });

  });

  describe('POST /api/spectras', function() {

    it('should route to spectra.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'spectraCtrl.create')
        ).to.have.been.calledOnce;
    });

  });

  describe('PUT /api/spectras/:id', function() {

    it('should route to spectra.controller.update', function() {
      expect(routerStub.put
        .withArgs('/:id', 'spectraCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('PATCH /api/spectras/:id', function() {

    it('should route to spectra.controller.update', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'spectraCtrl.update')
        ).to.have.been.calledOnce;
    });

  });

  describe('DELETE /api/spectras/:id', function() {

    it('should route to spectra.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'spectraCtrl.destroy')
        ).to.have.been.calledOnce;
    });

  });

});
