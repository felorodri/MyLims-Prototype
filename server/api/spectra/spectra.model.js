'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var SpectraSchema = new mongoose.Schema({
	
	request_code: { type: String, required: true, index: true, unique:true },
	status: { type: String, required: true, default: 'in progress' }, // Could be in progress/completed
	public: { type: Boolean, required: true, default: false },
	shared_to: { type: Array, default: [] },
	owner_id: { type: String, required: true, index: true },
	dissolve: { type: String },
	toxicity: { type: String },
	storage: { type: String },
	created_at: { type : Date, required: true, default: Date.now },

	experiments: 	[{ 	
						technique: { type: String, required: true },
						experiments: { type: Array, required: true },
						results: { type: Array, default: [] }
					}]

},{ strict: false });

export default mongoose.model('Spectra', SpectraSchema);
