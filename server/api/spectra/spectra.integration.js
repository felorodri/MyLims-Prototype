'use strict';

var app = require('../..');
import request from 'supertest';

var newSpectra;

describe('Spectra API:', function() {

  describe('GET /api/spectras', function() {
    var spectras;

    beforeEach(function(done) {
      request(app)
        .get('/api/spectras')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          spectras = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(spectras).to.be.instanceOf(Array);
    });

  });

  describe('POST /api/spectras', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/spectras')
        .send({
          name: 'New Spectra',
          info: 'This is the brand new spectra!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newSpectra = res.body;
          done();
        });
    });

    it('should respond with the newly created spectra', function() {
      expect(newSpectra.name).to.equal('New Spectra');
      expect(newSpectra.info).to.equal('This is the brand new spectra!!!');
    });

  });

  describe('GET /api/spectras/:id', function() {
    var spectra;

    beforeEach(function(done) {
      request(app)
        .get('/api/spectras/' + newSpectra._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          spectra = res.body;
          done();
        });
    });

    afterEach(function() {
      spectra = {};
    });

    it('should respond with the requested spectra', function() {
      expect(spectra.name).to.equal('New Spectra');
      expect(spectra.info).to.equal('This is the brand new spectra!!!');
    });

  });

  describe('PUT /api/spectras/:id', function() {
    var updatedSpectra;

    beforeEach(function(done) {
      request(app)
        .put('/api/spectras/' + newSpectra._id)
        .send({
          name: 'Updated Spectra',
          info: 'This is the updated spectra!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedSpectra = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedSpectra = {};
    });

    it('should respond with the updated spectra', function() {
      expect(updatedSpectra.name).to.equal('Updated Spectra');
      expect(updatedSpectra.info).to.equal('This is the updated spectra!!!');
    });

  });

  describe('DELETE /api/spectras/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/spectras/' + newSpectra._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when spectra does not exist', function(done) {
      request(app)
        .delete('/api/spectras/' + newSpectra._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
