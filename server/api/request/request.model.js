'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var RequestSchema = new mongoose.Schema({
  
	code: { type: String, required: true, index: true, unique:true },
	dissolve: { type: String },
	toxicity: { type: String },
	storage: { type: String },
	request_date: { type : Date, required: true, default: Date.now },
	fullfilled: { type: Boolean, required: true, default: false }, 
	country: { type: String, required: true },
	city: { type: String, required: true },
	submitted: { type: Boolean, required: true, default: false },
	additional_info: { type: String },
	owner_id: { type: String, required: true, index: true },
	experiments: { type: Array, required: true } 

});

RequestSchema.index({ country: 1, city: 1, submitted: 1 });

export default mongoose.model('Request', RequestSchema);
