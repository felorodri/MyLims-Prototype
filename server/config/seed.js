/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';

Thing.find({}).removeAsync()
  .then(() => {
    Thing.create({
      name: 'Development Tools',
      info: 'Integration with popular tools such as Bower, Grunt, Babel, Karma, ' +
             'Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, ' +
             'Stylus, Sass, and Less.'
    }, {
      name: 'Server and Client integration',
      info: 'Built with a powerful and fun stack: MongoDB, Express, ' +
             'AngularJS, and Node.'
    }, {
      name: 'Smart Build System',
      info: 'Build system ignores `spec` files, allowing you to keep ' +
             'tests alongside code. Automatic injection of scripts and ' +
             'styles into your index.html'
    }, {
      name: 'Modular Structure',
      info: 'Best practice client and server structures allow for more ' +
             'code reusability and maximum scalability'
    }, {
      name: 'Optimized Build',
      info: 'Build process packs up your templates as a single JavaScript ' +
             'payload, minifies your scripts/css/images, and rewrites asset ' +
             'names for caching.'
    }, {
      name: 'Deployment Ready',
      info: 'Easily deploy your app to Heroku or Openshift with the heroku ' +
             'and openshift subgenerators'
    });
  });

User.find({}).removeAsync()
  .then(() => {
    User.createAsync({
      provider: 'local',
      name: 'Test User',
      email: 'test@example.com',
      password: 'test'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Julian Rodriguez',
      email: 'felorodri@gmail.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Stephanie Stephen',
      email: 'sstephens6@wufoo.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Sandra Gomez',
      email: 'sgomez5@intel.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Randy Gilbert',
      email: 'rgilbert4@skyrock.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Carles Stones',
      email: 'cstone3@sohu.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Raquel Gomez',
      email: 'rgomez2@dailymotion.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Robert Pierce',
      email: 'rpierce1@about.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Andres Boyaca',
      email: 'aboyd0@ca.gov',
      password: 'password'
    }, {
      provider: 'local',
      name: 'David Guyen',
      email: 'dnguyenc@posterous.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Alexander Buitrago',
      email: 'malexanderb@wikia.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Luisa Smith',
      email: 'lsmitha@ask.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Michael Schmidt',
      email: 'mschmidt9@wisc.edu',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Charlie Simmons',
      email: 'csimmons8@ehow.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Hilarie Dunney',
      email: 'hdunne@cyberchimps.com',
      password: 'password'
    }, {
      provider: 'local',
      name: 'Jackson Thomas',
      email: 'jthomasd@clickbank.net',
      password: 'password'
    }
    )
    .then(() => {
      console.log('finished populating users');
    });
  });
            