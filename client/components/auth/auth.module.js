'use strict';

angular.module('myLimsPrototypeApp.auth', [
  'myLimsPrototypeApp.constants',
  'myLimsPrototypeApp.util',
  'ngCookies',
  'ngRoute'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
