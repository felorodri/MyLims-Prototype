'use strict';

(function() {

class MainController {

  constructor($http) {
    this.$http = $http;
    this.awesomeThings = [];
    this.registered_users = 0;
    this.registered_spectras = 0;
    this.registered_public_spectras = 0;

    $http.get('/api/things').then(response => {
      this.awesomeThings = response.data;
    });

    $http.get('/api/spectras/public/count').then(response => {
      this.registered_public_spectras = response.data;
    });

    $http.get('/api/spectras/count').then(response => {
      this.registered_spectras = response.data;
    });

    $http.get('/api/users/count').then(response => {
      this.registered_users = response.data;
    });
    
  }

  addThing() {
    if (this.newThing) {
      this.$http.post('/api/things', { name: this.newThing });
      this.newThing = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete('/api/things/' + thing._id);
  }
}

angular.module('myLimsPrototypeApp')
  .controller('MainController', MainController);

})();