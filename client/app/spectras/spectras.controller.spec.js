'use strict';

describe('Controller: SpectrasCtrl', function () {

  // load the controller's module
  beforeEach(module('myLimsPrototypeApp'));

  var SpectrasCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SpectrasCtrl = $controller('SpectrasCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).to.equal(1);
  });
});
