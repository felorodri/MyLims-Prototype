'use strict';

angular.module('myLimsPrototypeApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/spectras', {
        templateUrl: 'app/spectras/spectras.html',
        controller: 'SpectrasCtrl'
      });
  });
