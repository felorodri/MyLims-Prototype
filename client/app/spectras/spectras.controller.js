'use strict';

angular.module('myLimsPrototypeApp')
  .controller('SpectrasCtrl', function ($scope, $http, $modal){

  	$scope.spectras = null;
  	$scope.status = 'Loading data, this would take a while...';
  	
  	$http.get('/api/spectras/public').then(response => {
      $scope.spectras = response.data;
      $scope.status = "";
    });

    $scope.openDetails = function(spectra){
    	var modalInstance = $modal.open({
			templateUrl: 'spectra.html',
			controller: 'SpectraCtrl',
			resolve: {
				owner: function () {
					return spectra.owner_id;
				},
				requestCode: function () {
					return spectra.request_code;
				},
				techniques: function () {
					return spectra.techniques;
				},
				experiments: function () {
					return spectra.experiments;
				},
				solvents: function () {
					// return $scope.solvents;
				},
				toxicity: function () {
					// return $scope.toxicity;
				},
				storage: function () {
					// return $scope.storage;
				},
				status: function () {
					return spectra.status;
				}
			}
		});
    };

});


angular.module('myLimsPrototypeApp')
  .controller('SpectraCtrl', function($scope, $modalInstance, owner, techniques, experiments, solvents, toxicity, storage, requestCode, status){
	
	$scope.owner = owner;
	$scope.request_code = requestCode;
	$scope.techniques = techniques;
	$scope.experiments = experiments;
	$scope.solvents = solvents;
	$scope.toxicity = toxicity;
	$scope.storage = storage;
	$scope.status = status;
	// $scope.form = {};

	//console.log(cities);

	$scope.ok = function () {
		$modalInstance.close('Ok');
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('Cancel');
	};
});	
