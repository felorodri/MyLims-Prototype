'use strict';

angular.module('myLimsPrototypeApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/request', {
        templateUrl: 'app/request/request.html',
        controller: 'RequestCtrl'
      });
  });
