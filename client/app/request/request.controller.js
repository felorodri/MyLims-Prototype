'use strict';

angular.module('myLimsPrototypeApp')
  .controller('RequestCtrl', function ($scope, $modal, $http) {
    
    $scope.form = {};
	$scope.items = [];
	
	$scope.cities = [
		{ name: 'Cali', country: 'Colombia', id: 'Cali' },
		{ name: 'Bogota', country: 'Colombia', id: 'Bogota' },
		{ name: 'Paris', country: 'Francia', id: 'Paris' },
		{ name: 'Berne', country: 'Switzerland', id: 'Berne' }
	];

	$scope.techniques = [ 
		{ name: 'NMR', id:'NMR' },
		{ name: 'IR', id:'IR' },
		{ name: 'MASS', id: 'MASS'}
	];

	$scope.experiments = [
		{ name: '1H', id:'1H' },
		{ name: 'Cosy', id:'Cosy' },
		{ name: 'Tocsy', id:'Tocsy' },
		{ name: 'Noesy', id:'Noesy' },
		{ name: 'Cosy with fixed SW 16 ppm (for non deuterated solvents)', id:'Cosy_SW16ppm' },
		{ name: 'Cosy with fixed SW 20 ppm', id:'Cosy_SW20ppm' },
		{ name: '13C', id:'13C' },
		{ name: 'Dept135', id:'Dept135' },
		{ name: 'Edited-HSQC', id:'Edited-HSQC' },
		{ name: 'HSQC-TOCSY (2D)', id:'HSQC-TOCSY' }
	];

	$scope.phases = [

		{ name: 'NEAT', id:'NEAT' },
		{ name: 'KBr', id:'KBr' },
		{ name: 'Difuse', id:'Difuse' },
		{ name: 'ATR', id:'ATR' }
	];

	$scope.solvents = [
			{ name: 'H2O', id:'H2O' },
			{ name: 'CDCI3', id:'CDCI3'}
	];

	$scope.toxicity = [
			{ name: 'Yes', id:'Yes' },
			{ name: 'No', id:'No'}
	];

	$scope.storage = [
			{ name: 'Room temperature', id:'Room temperature' },
			{ name: 'Keep cold', id:'Keep cold'}
	];
    

    $scope.addRequest = function () {

		var modalInstance = $modal.open({
			templateUrl: 'addRequest.html',
			controller: 'AddRequestCtrl',
			resolve: {
				items: function () {
					return $scope.items;
				},
				cities: function () {
					return $scope.cities;
				},
				techniques: function () {
					return $scope.techniques;
				},
				experiments: function () {
					return $scope.experiments;
				},
				solvents: function () {
					return $scope.solvents;
				},
				toxicity: function () {
					return $scope.toxicity;
				},
				storage: function () {
					return $scope.storage;
				},
				phases: function () {
					return $scope.phases;
				},
			}
		});

		modalInstance.result.then(function (form) {
			$scope.form = form;
			$scope.items.push(form);
		}, function () {
			//console.log('Closed');
		});
	};

	$scope.sendResquests = function(){

		if (!($scope.items.length == 0)){

			var requests_to_check = $scope.items.length;
			var requests_checked = 0;

			$http.get('/api/users/me').then(response => {
				
				if (response.status === 200){

					var user = response.data.email;

					for (var i = 0; i < $scope.items.length; i++) {
				
						var request_doc = {};
						var exps = {};

						request_doc.city = $scope.items[i].city;
						request_doc.code = $scope.items[i].code;
						request_doc.storage = $scope.items[i].storage;
						request_doc.toxicity = $scope.items[i].toxicity;

						for (var k = 0; k < $scope.cities.length; k++) {
							
							if ($scope.cities[k].name === $scope.items[i].city){
								request_doc.country = $scope.cities[k].country;
								break;
							}

						};

						exps.technique = $scope.items[i].technique;
						

						if ($scope.items[i].experiment !== undefined) exps.experiments = $scope.items[i].experiment;
						if ($scope.items[i].additional_info !== undefined) request_doc.additional_info = $scope.items[i].additional_info;
						if ($scope.items[i].dissolve !== undefined) request_doc.dissolve = $scope.items[i].dissolve;

						request_doc.experiments = [exps];
						request_doc.owner_id = user;

						$http.post('/api/requests', request_doc);
						requests_checked ++;

						if (requests_checked === requests_to_check) {

							swal("Done!", "All requests succesfully placed!", "success")
							$scope.items = [];

						}
					
					};

				}else{

					sweetAlert("Oops...", "Something went wrong, try again later!", "error");
				}

	    	});

		}
		
	};

  });

angular.module('myLimsPrototypeApp')
  .controller('AddRequestCtrl', function($scope, $modalInstance, cities, techniques, experiments, solvents, toxicity, storage, phases){
	$scope.cities = cities;
	$scope.techniques = techniques;
	$scope.experiments = experiments;
	$scope.solvents = solvents;
	$scope.toxicity = toxicity;
	$scope.storage = storage;
	$scope.phases = phases;
	$scope.form = {};
	$scope.show_experiments = false;
	$scope.show_phases = false;

	//console.log(cities);

	$scope.showExperiments = function (){

		if ($scope.form.technique === 'NMR') {
			$scope.show_experiments = true;
		}else{
			$scope.show_experiments = false;
		}

		if ($scope.form.technique === 'IR') {
			$scope.show_phases = true;
		}else{
			$scope.show_phases = false;
		}

	}

	$scope.ok = function () {
		
		if($scope.form.code !== undefined && $scope.form.city !== undefined && $scope.form.technique !== undefined){
			$modalInstance.close($scope.form);	
		}else{

			sweetAlert('Oops...','Did you fill all the fields?', 'error');

		}
		
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});	