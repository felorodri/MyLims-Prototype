'use strict';

angular.module('myLimsPrototypeApp')
  .controller('DashboardCtrl', function ($scope, $http, $modal) {
    
    $scope.user = {};
    $scope.owned_spectras = [];
    $scope.shared_spectras = [];

    $http.get('/api/users/me').then(response => {

    	$scope.user=response.data;

    	$http.get('/api/spectras/' + response.data.email).then(response => {
    		$scope.owned_spectras = response.data;
    	});

    	$http.get('/api/spectras/shared/' + response.data.email).then(response => {
    		$scope.shared_spectras = response.data;
    	});

    });


    $scope.showSpectraInfo = function (spectra, mine) {

        var modalInstance = $modal.open({
        
        	templateUrl: 'spectra_info.html',
          	controller: 'spectraInfoCtrl',
          	resolve: {
            	spectra: function () {
              		return spectra;
            	},
            	mine: function() {
            		return mine;
            	}
          	}
        });

        modalInstance.result.then(function (id) {
          $scope.id = id;
          //$scope.items.push(form);
        }, function () {
          
          //console.log('Closed');

        });
     
    };

});


angular.module('myLimsPrototypeApp')
.controller('spectraInfoCtrl', function($scope, $http, $modal, $modalInstance, spectra, mine){
	$scope.mine = mine;
  $scope.id = spectra._id;
  $scope.request_code = spectra.request_code;
  $scope.technique = spectra.technique;
  $scope.experiments = spectra.experiments;
  $scope.dissolve = spectra.dissolve;
  $scope.request_date = spectra.request_date;
  $scope.experiment_date = spectra.experiment_date;
  $scope.toxicity = spectra.toxicity;
  $scope.storage = spectra.storage;

  
  $scope.ok = function () {
   	$modalInstance.close($scope.id);
  };

  $scope.getResult = function (id) {

  	$http.get('/api/results/' + id).then(response => {

   		var modalInstance = $modal.open({
        
       		templateUrl: 'result_info.html',
       		controller: 'resultInfoCtrl',
       		resolve: {
         		result: function () {
         			return response.data;
         		}
       		}
     	});

     	modalInstance.result.then(function (id) {
     		$scope.id = id;
       		//$scope.items.push(form);
   		}, function () {
          
     	});

   	});

 	};

  $scope.shareSpectra = function (id) {

  	// console.log(id);

  	swal({ 	
  		title: "Share spectra",
  		text: "Write an email address or multiple coma separated email addresses",
  		type: "input",
  		showCancelButton: true,
  		closeOnConfirm: false,
  		showLoaderOnConfirm: true,
  		animation: "slide-from-top",
  		inputPlaceholder: "email address(es)",
  	  }, 	function(inputValue){

				if (inputValue === false) return false;
				if (inputValue === "") {
				    swal.showInputError("You need to write something!");
				    return false
				}

				var emails = inputValue.replace(/ /g, '');
				var email_list = emails.split(",");
  					
				$http.put('/api/spectras/share/' + id, {emails: email_list}).then(response => {
					swal("Done!", "Spectra shared succesfully!", "success")
				});
			    		
			}	
	  );
  };
  
}); 


angular.module('myLimsPrototypeApp')
.controller('resultInfoCtrl', function($scope, $modalInstance, result){

	$scope.result = result;

	$scope.ok = function () {
    	$modalInstance.close($scope.id);
  	};

});