'use strict';

angular.module('myLimsPrototypeApp')
  .controller('SearchCtrl', function ($scope, $http, $modal) {
    $scope.input = "";
    $scope.query={};
    $scope.mine = true;
    $scope.shared = false;
    $scope.public = false;
    $scope.user_id = "";
    $scope.in_result_data = false;
    $scope.query.on_result = [];
    $scope.query.on_data = [];
    $scope.button = false;

    $scope.spectras = null;
  	$scope.status = '';

    $http.get('/api/users/me').then(response => {
    	$scope.user_id=response.data.email;
    });

    $scope.search = function (){
    	
    	$scope.button = true;
    	var mongoDB_query = {};
    	mongoDB_query.user_id = $scope.user_id;
    	mongoDB_query.type = {mine: $scope.mine, shared: $scope.shared, public: $scope.public};
    	mongoDB_query.in_result_data = $scope.in_result_data;

    	try{
    		var query_json = JSON.parse($scope.input);
    		mongoDB_query.query = query_json;

    		$http.post('/api/results/search', mongoDB_query).then(function (response) {

    			// console.log(response);
    			$scope.spectras = response.data;
    			$scope.button = false;

    			}, function (response){
    				// console.log(response);
    				sweetAlert(response.statusText, 'Sorry, could not process your query. ' + response.data, "error");
    				$scope.button = false;
    			} 
    		);

    	}catch(err){
    		sweetAlert("Oops...", "Could not parse your query, check it!", "error");
    		$scope.button = false;
    	}
    	
    };

    $scope.showSpectraInfo = function (spectra, mine) {

        var modalInstance = $modal.open({
        
        	templateUrl: 'spectra_info.html',
          	controller: 'spectraInfoCtrl',
          	resolve: {
            	spectra: function () {
              		return spectra;
            	},
            	mine: function() {
            		return mine;
            	}
          	}
        });

        modalInstance.result.then(function (id) {
          $scope.id = id;
          //$scope.items.push(form);
        }, function () {
          
          //console.log('Closed');

        });
     
    };

  });

angular.module('myLimsPrototypeApp')
.controller('spectraInfoCtrl', function($scope, $http, $modal, $modalInstance, spectra, mine){
	$scope.mine = mine;
  $scope.id = spectra._id;
  $scope.request_code = spectra.request_code;
  $scope.technique = spectra.technique;
  $scope.experiments = spectra.experiments;
  $scope.dissolve = spectra.dissolve;
  $scope.request_date = spectra.request_date;
  $scope.experiment_date = spectra.experiment_date;
  $scope.toxicity = spectra.toxicity;
  $scope.storage = spectra.storage;

  
  $scope.ok = function () {
   	$modalInstance.close($scope.id);
  };

  $scope.getResult = function (id) {

  	$http.get('/api/results/' + id).then(response => {

   		var modalInstance = $modal.open({
        
       		templateUrl: 'result_info.html',
       		controller: 'resultInfoCtrl',
       		resolve: {
         		result: function () {
         			return response.data;
         		}
       		}
     	});

     	modalInstance.result.then(function (id) {
     		$scope.id = id;
       		//$scope.items.push(form);
   		}, function () {
          
     	});

   	});

 	};

  $scope.shareSpectra = function (id) {

  	// console.log(id);

  	swal({ 	
  		title: "Share spectra",
  		text: "Write an email address or multiple coma separated email addresses",
  		type: "input",
  		showCancelButton: true,
  		closeOnConfirm: false,
  		showLoaderOnConfirm: true,
  		animation: "slide-from-top",
  		inputPlaceholder: "email address(es)",
  	  }, 	function(inputValue){

				if (inputValue === false) return false;
				if (inputValue === "") {
				    swal.showInputError("You need to write something!");
				    return false
				}

				var emails = inputValue.replace(/ /g, '');
				var email_list = emails.split(",");
  					
				$http.put('/api/spectras/share/' + id, {emails: email_list}).then(response => {
					swal("Done!", "Spectra shared succesfully!", "success")
				});
			    		
			}	
	  );
  };
  
}); 


angular.module('myLimsPrototypeApp')
.controller('resultInfoCtrl', function($scope, $modalInstance, result){

	$scope.result = result;

	$scope.ok = function () {
    	$modalInstance.close($scope.id);
  	};

});