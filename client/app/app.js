'use strict';

angular.module('myLimsPrototypeApp', [
  'myLimsPrototypeApp.auth',
  'myLimsPrototypeApp.admin',
  'myLimsPrototypeApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'validation.match',
  'datatables',
  'jsonFormatter'
])
  .config(function($routeProvider, $locationProvider) {
    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  });
