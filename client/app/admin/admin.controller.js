'use strict';

angular.module('myLimsPrototypeApp')
  .controller('AdminController', function ($scope, $http, $modal, DTOptionsBuilder){

  	$scope.requests = null;
  	$scope.status = 'Loading data, this would take a while...';
  	$scope.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('aaSorting', []);
  	
  	$http.get('/api/requests').then(response => {
      $scope.requests = response.data;
      $scope.status = "";
    });


    $scope.submitRequest = function (request) {

    	var spectra = {};

    	spectra.request_code = request.code + '-' + (Date.now() / 1000 | 0).toString();
    	spectra.owner_id = request.owner_id;
    	spectra.experiments = request.experiments;
    	spectra.shared_to=[];

    	if (request.dissolve !== undefined)	spectra.dissolve = request.dissolve;
		if (request.toxicity !== undefined) spectra.toxicity = request.toxicity;
		if (request.storage !== undefined) spectra.storage = request.storage;
    		
    	$http.post('/api/spectras', spectra);
		swal("Done!", "Request succesfully submitted!", "success")

    };

   
});