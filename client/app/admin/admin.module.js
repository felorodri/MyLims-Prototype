'use strict';

angular.module('myLimsPrototypeApp.admin', [
  'myLimsPrototypeApp.auth',
  'ngRoute'
]);
