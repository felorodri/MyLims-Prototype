//This always goes here

var mongoose	= require('mongoose'),
	Schema		= mongoose.Schema;

// Data schema definition

var SpectraSchema = Schema({
	
	request_code: { type: String, required: true, index: true, unique:true },
	status: { type: String, required: true, default: 'in progress' }, // Could be in progress/completed
	public: { type: Boolean, required: true, default: false },
	shared_to: { type: Array, default: [] },
	owner_id: { type: String, required: true, index: true },

	experiments: 	[{ 	
						technique: { type: String, required: true },
						experiments: { type: Array, required: true },
						results: { type: Array, default: [] },
						date: { type : Date, required: true, default: Date.now },
					}]

});

// Data model creation

var model	= mongoose.model('spectras', SpectraSchema);

// Module exportation (Make it visible from outside):

module.exports	= {
	Spectra : model
};