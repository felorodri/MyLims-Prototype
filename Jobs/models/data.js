//This always goes here

var mongoose	= require('mongoose'),
	Schema		= mongoose.Schema;

// Data schema definition

var dataSchema = new Schema({

	text: { type: String, required: true },
	
});


// Data model creation

var model	= mongoose.model('datas', dataSchema);

// Module exportation (Make it visible from outside):

module.exports	= {
	Data : model
};