//This always goes here

var mongoose	= require('mongoose'),
	Schema		= mongoose.Schema;

// Data schema definition

var resultSchema = new Schema({
	
	// data: { type: Schema.Types.ObjectId, required: true }
	
}, {strict: false});


// Data model creation

var model	= mongoose.model('results', resultSchema);

// Module exportation (Make it visible from outside):

module.exports	= {
	Result : model
};
