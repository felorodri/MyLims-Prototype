
/**
 * Created by Julián Rodríguez for MyLims-Prototype
 * January 18th 2016
 */


// Required dependencies:

var mongoose = require('mongoose');
var fs = require('fs');
var mv = require('mv');


// Required config files:

var mongo_db  = require('./config/mongodb.json');


// Required models:

var dataModel    = require('./models/data').Data;
var resultModel  = require('./models/result').Result;


// Variable initializer:

var connectionString = "";
var results_folder = '/home/ghost/Desktop/data/unprocessed/';
var processed_folder = '/home/ghost/Desktop/data/processed/';
var processed_files = [];


// Enviroment configuration

if (!(process.env.NODE_ENV === 'production')) {
    
  // Mongoose configuration:
  mongoose.connect('mongodb://' + mongo_db.host + '/' + mongo_db.db_development);
   
}else{

  // Mongoose configuration:
  mongoose.connect('mongodb://' + mongo_db.host + '/' + mongo_db.db_development);
  
}


/*************************************************************************************/
/*                           RESULTS PROCESSING FUNCTIONS                            */
/*************************************************************************************/

var files_to_check = 0;
var checked_files = 0;
var files_to_move = 0;
var moved_files = 0;

function readFiles(dirname, onFileContent, onError) {

  	fs.readdir(dirname, function(err, filenames) {
    	
    	if (err) {
    		onError(err);
      		return;
    	}
    	
    	if (!(filenames.length == 0)) {

    		files_to_check = filenames.length;
    		filenames.forEach(function(filename) {

		      	fs.readFile(dirname + filename, 'utf-8', function(err, content) {
		        	
		        	if (err) {
		        		console.log(filenames);
		         		onError(err);
		          		return;
		        	}

		 			onFileContent(filename, content);
	      		});
    	
    		});

    	}else {
    		onError('No files to proceed...');
    		onError('Results job exiting now.');
    		process.exit();
    	}

  	});

}

function dataExtraction(filename, content, dataId, callback){
	

	var content_array = content.match(/[^\r\n]+/g);


	// RESULT FILE RELEVANT VALUES EXTRACTION
	
	var experiment_values_positions = [];
	var acquisition_params_positions = [];
	var processing_params_positions = [];
	var channel_one_params_positions = [];

	var counter = 0;

	content_array.forEach(function (array_value){
		
		if (stringStartsWith(array_value, "##.")) {

			experiment_values_positions.push(counter);
			    
		}else if(stringStartsWith(array_value, "$$ F2 - Acquisition")){

			for (var i = 1; i < 20; i++) {
				acquisition_params_positions.push(counter+i);
			};

		}else if(stringStartsWith(array_value,"$$ F2 - Processing parameters")){

			for (var j = 1; j < 8; j++) {
				processing_params_positions.push(counter+j);
			};

		}else if (stringStartsWith(array_value,"$$ ======== CHANNEL f1 ========")){

			for (var k = 1; k < 5; k++) {
				channel_one_params_positions.push(counter+k);
			};

		}

		counter++;

	});

	
	// EXPERIMENT VALUES EXTRACTION

	var experiment_values = {};
	counter = 0;

	experiment_values_positions.forEach(function (array_value){

		var text_line = content_array[array_value].replace('##.', '');
		var res = text_line.split("=");

		if (counter == 0 || counter == 4 || counter == 5) {

			res[1]=Number(res[1]);

		}

		experiment_values[res[0]] = res[1];

		counter++;
		
	});


	// ACQUISITION PARAMETERS VALUES EXTRACTION

	var acquisition_params = {};
	
	acquisition_params_positions.forEach(function (array_value){

		var text_line = content_array[array_value].replace('$$', '');
		var res = text_line.split(" ");
		var tmp = [];

		for (var i = 0; i < res.length; i++) {
			
			if (!(res[i] == "" || res[i] == " ")) {

				tmp.push(res[i]);
			};
				
		};

		if (tmp[0] == 'Date_' || tmp[0] == 'INSTRUM' || tmp[0] == 'PROBHD' || tmp[0] == 'PULPROG' || tmp[0] == 'SOLVENT'){

			if (tmp[0] == 'Date_') {
				
				var date = tmp[1];
				var new_date = "";

				for (var k = 0; k < date.length; k++) {
						
					new_date = new_date + date[k];
					
					if (k == 3 || k == 5 ){
						new_date = new_date+"-";
					}

				};

				acquisition_params.Date = new Date(new_date);

			}else {

				if ( !(tmp[0] == 'PROBHD')) {
					acquisition_params[tmp[0]] = tmp[1];		
				};

			}

		}else{
			acquisition_params[tmp[0]] = Number(tmp[1]); 
		}
		
	});


	// PROCESSING PARAMETERS VALUES EXTRACTION

	var processing_params = {};

	processing_params_positions.forEach(function (array_value){
		
		var text_line = content_array[array_value].replace('$$', '');
		var res = text_line.split(" ");
		var tmp = [];

		for (var i = 0; i < res.length; i++) {
			
			if (!(res[i] == "" || res[i] == " ")) {

				tmp.push(res[i]);
			};
				
		};

		if (tmp[0] == 'WDW'){

			processing_params[tmp[0]] = tmp[1];

		}else{

			processing_params[tmp[0]] = Number(tmp[1]);

		}
		
	});


	// CHANNEL ONE VALUES EXTRACTION
	
	var channel_one_values = {};

	channel_one_params_positions.forEach(function (array_value){
		
		var text_line = content_array[array_value].replace('$$', '');
		var res = text_line.split(" ");
		var tmp = [];

		for (var i = 0; i < res.length; i++) {
			
			if (!(res[i] == "" || res[i] == " ")) {

				tmp.push(res[i]);

			};
				
		};

		if (tmp[0] == 'NUC1'){
			
			channel_one_values[tmp[0]] = tmp[1];

		}else{
			
			channel_one_values[tmp[0]] = Number(tmp[1]);

		}
		
	});


	// CREATING THE DOCUMENT TO STORE IN MONGODB
	
	var document_to_store = {};

	document_to_store.experiment_values = experiment_values;
	document_to_store.acquisition_params = acquisition_params;
	document_to_store.processing_params = processing_params;
	document_to_store.channel_one_values = channel_one_values;

	callback(document_to_store, dataId);
	
}

function storeData(filename, content){

	var newData = new dataModel();

	newData.text = content;
	newData.save(function(err) {
		      
		if(!err) {
		    
			dataExtraction(filename,content, newData._id, function (extractedData, dataId){

				var newResult = new resultModel();

				newResult.set('exp_values', extractedData.experiment_values);
				newResult.set('acq_params', extractedData.acquisition_params);
				newResult.set('proc_params', extractedData.processing_params);
				newResult.set('ch1_values', extractedData.channel_one_values);
				newResult.set('data', mongoose.Types.ObjectId(dataId));

				
				newResult.save(function (err){

					if (!err){

						checked_files++;

						processed_files.push(filename);

						if (checked_files == files_to_check) {

							errorHandler('All files checked!');
							files_to_move = processed_files.length;
							moveProcessedFiles(results_folder, processed_folder, processed_files);

						}

					}else{

						errorHandler('File '+ filename + ' not processed');
						checked_files++;

						if (checked_files == files_to_check) {

							errorHandler('All files checked!');
							files_to_move = processed_files.length;
							moveProcessedFiles(results_folder, processed_folder, processed_files);
							
						}

					}

				});

			});

		} else {
		    errorHandler(err);
		}
			      
	});

}

function moveProcessedFiles(source, dest, fileList){

	if (!(fileList.length <= 0)) {

		fileList.forEach(function (file){

			mv(source+file, dest+file, function(err) {
	    		
	    		moved_files++;

	    		if (moved_files == files_to_move) {

	    			errorHandler('Processed files moved!');
	    			errorHandler('Results job exiting now.');
	    			process.exit();

	    		};

			});

		});
	
	}else{

		errorHandler('No files to move...');
	    errorHandler('Results job exiting now.');
	    process.exit();

	}

}

function errorHandler(err){
	console.log(err);
}


function stringStartsWith (string, prefix) {
    return string.slice(0, prefix.length) == prefix;
}




/*************************************************************************************/
/*                            	  JOBS EXECUTION                                     */
/*************************************************************************************/


readFiles(results_folder, storeData, errorHandler);
// moveProcessedFiles('/home/ghost/Desktop/197/resource_393670', '/home/ghost/Desktop/197/processed/resource_393670');